package com.example.hello10;




import java.util.List;



import android.app.Activity;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends Activity
{
	
	public TelephonyManager telephonyManager;
	
	
	public TextView txtSignalStr;
	public TextView GSMid;
	public static TextView n1;
	public static TextView n2;
	public static final String TAG = "TelephonyDemoActivity";
	myPhoneStateListener plistener;
	WifiManager wifiManager;
	int level =0;
	@Override
    protected void onCreate(final Bundle savedInstanceState)
    {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
   
      	 
        plistener= new 	myPhoneStateListener();
        txtSignalStr = (TextView)findViewById(R.id.signalStrength);
        GSMid  = (TextView)findViewById(R.id.wifi);
        n1  = (TextView)findViewById(R.id.nn);
        n2  = (TextView)findViewById(R.id.mm);
    
        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        

        telephonyManager.listen(plistener,
                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS  | PhoneStateListener.LISTEN_CELL_LOCATION | PhoneStateListener.LISTEN_CELL_INFO);

        Context context=getApplicationContext();
     	 wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
       

    
 
    
  
	}

	public class myPhoneStateListener extends PhoneStateListener{
	
	 public int signalStrengthValue;
	
   
    @Override
    public void onCellLocationChanged (CellLocation location) {
    	super.onCellLocationChanged(location);
    	
        
        
        StringBuffer str = new StringBuffer();
        // GSM
       location = telephonyManager.getCellLocation();
       if (location == null) {
           GSMid.setText("no cell");
        }
       
           
        if (location instanceof GsmCellLocation) {
 	       GsmCellLocation loc = (GsmCellLocation) location;

        
            str.append("gsm ");
            str.append(loc.getCid() & 0xffff);
            str.append(" ");
            str.append(loc.getLac() & 0xffff);
            Log.d(TAG, str.toString());
            String cellId = Integer.toHexString(loc.getCid());
            String cellLac = Integer.toHexString(loc.getLac());
            GSMid.setText("id + loc : " + str + cellId + cellLac);
            }
        else if (location instanceof CdmaCellLocation){
        	 CdmaCellLocation cdma = (CdmaCellLocation) location;

	            str.append("gsm ");
	            str.append(cdma.getBaseStationId() & 0xffff);
	            str.append(" ");
	            str.append(cdma.getNetworkId() & 0xffff);
	            Log.d(TAG, str.toString());
	            String cellId = Integer.toHexString(cdma.getBaseStationId());
	            String cellLac = Integer.toHexString(cdma.getNetworkId());
	            GSMid.setText("id + loc : " + str + cellId + cellLac);
        }
        
        n1  = (TextView)findViewById(R.id.nn);
        StringBuilder telephonyInfo = new StringBuilder();

     	List<NeighboringCellInfo> mNeighboringCellInfo = telephonyManager.getNeighboringCellInfo();

		if (!mNeighboringCellInfo.isEmpty()) {
			int i = 1;
			for (NeighboringCellInfo cellInfo : mNeighboringCellInfo) {
				Log.i(TAG,
						"Neighbor CellInfo No." + i + " LAC:"
								+ cellInfo.getLac() + ", CID:"
								+ cellInfo.getCid() + ", RSSI:"
								+ cellInfo.getRssi());
				i++;
				telephonyInfo.append("Neighbor CellInfo No." + i
						+ ", Location Area Code: " + (cellInfo.getLac()& 0xffff)
						+ ", Cell ID: " + (cellInfo.getCid()& 0xffff)
						+ ", Signal Strength: " + (cellInfo.getRssi() *2-113)+ "\r\n");
			}
		} else
			telephonyInfo.append("No Neighbor Cell Info!");

		n1.setText(telephonyInfo.toString());
		      String lis="";
		  	if(wifiManager.isWifiEnabled()){
		  		if(wifiManager.startScan()){
		  		// List available APs
		  		List<ScanResult> scans = wifiManager.getScanResults();
		  		if(scans != null && !scans.isEmpty()){
		  		for (ScanResult scan : scans) {
		  		int level = WifiManager.calculateSignalLevel(scan.level, 20);
		  		
		  		}
		  		int rssi = wifiManager.getConnectionInfo().getRssi();
		        int level = WifiManager.calculateSignalLevel(rssi, 5);
		        String s= wifiManager.getConnectionInfo().getSSID();
		      
		        for (ScanResult result : scans) {
		          lis +=  "name: "+result.SSID +", strength :"+result.level+"\n";
		          
		        }
		  		}
		  		}
		  		}
		  	 n2.setText(lis);
		  	
    }
    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
     
        onCellLocationChanged(telephonyManager.getCellLocation());
        
    
        if (signalStrength.isGsm()) // true if this is for GSM
        {   
            if (signalStrength.getGsmSignalStrength() != 99)
                signalStrengthValue = signalStrength.getGsmSignalStrength() * 2 - 113;//Get the GSM Signal Strength, valid values are (0-31, 99) 
           
            else
                signalStrengthValue = signalStrength.getGsmSignalStrength();
        } else {
            signalStrengthValue = signalStrength.getCdmaDbm();//Get the CDMA RSSI value in dBm
        }
        
        
        n1  = (TextView)findViewById(R.id.nn);
        StringBuilder telephonyInfo = new StringBuilder();
      

     	List<NeighboringCellInfo> mNeighboringCellInfo = telephonyManager.getNeighboringCellInfo();

		if (!mNeighboringCellInfo.isEmpty()) {
			int i = 1;
			for (NeighboringCellInfo cellInfo : mNeighboringCellInfo) {
				Log.i(TAG,
						"Neighbor CellInfo No." + i + " LAC:"
								+ cellInfo.getLac() + ", CID:"
								+ cellInfo.getCid() + ", RSSI:"
								+ cellInfo.getRssi());
				i++;
				telephonyInfo.append("Neighbor CellInfo No." + i
						+ ", Location Area Code: " + (cellInfo.getLac()& 0xffff)
						+ ", Cell ID: " + (cellInfo.getCid()& 0xffff)
						+ ", Signal Strength: " + (cellInfo.getRssi() *2-113)+ "\r\n");
			}
		} else
			telephonyInfo.append("No Neighbor Cell Info!");

		n1.setText(telephonyInfo.toString());
	
    txtSignalStr.setText("Signal Strength : " + signalStrengthValue);
  
   
   
  	
    String lis="";
  	if(wifiManager.isWifiEnabled()){
  		if(wifiManager.startScan()){
  		// List available APs
  		List<ScanResult> scans = wifiManager.getScanResults();
  		if(scans != null && !scans.isEmpty()){
  		for (ScanResult scan : scans) {
  		int level = WifiManager.calculateSignalLevel(scan.level, 20);
  		
  		}
  		int rssi = wifiManager.getConnectionInfo().getRssi();
        int level = WifiManager.calculateSignalLevel(rssi, 5);
        String s= wifiManager.getConnectionInfo().getSSID();
      
        for (ScanResult result : scans) {
          lis +=  "name: "+result.SSID +", strength :"+result.level+"\n";
          
        }
  		}
  		}
  		}
  	 n2.setText(lis);
  
    }

}


		
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



}

